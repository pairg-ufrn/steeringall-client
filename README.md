Steering All
============

The classical racing game for nowadays racers

![Steering All Beta 0.2.5](https://j.gifs.com/1jvL5G.gif)


Setting up for development
--------------------------

You are going to need:
* Python 2.7 ([Download here](https://www.python.org/))
* Pygame 1.9.2 ([Download here](https://www.pygame.org/))

For you guys running debian based distros, run the following command and
you're good to go
```
$ sudo apt-get install python-pygame
```

VCS Branching model
-------------------

Steering all uses the branching model proposed by Vincent Driessen, available at
[A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)


Running the game
----------------
Use the following commands to run the game
```
$ python steering-all.py # removes all .pyc files before running
$ python game.py # no cache cleaning
```
