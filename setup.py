import os
import sys
from distutils.core import setup

import py2exe

# save the orginal before we edit it
origIsSystemDLL = py2exe.build_exe.isSystemDLL


def is_system_dll(pathname):
    # checks if the freetype and ogg dll files are being included
    dlls = ("libfreetype-6.dll", "libogg-0.dll", "sdl_ttf.dll")
    if os.path.basename(pathname).lower() in dlls:
        return 0

    # return the orginal function
    return origIsSystemDLL(pathname)

# override the default function with this one
py2exe.build_exe.isSystemDLL = is_system_dll

sys.argv.append('py2exe')

build_path = os.path.join(os.getcwd(), 'release', 'py2exe')

setup(
    options={'py2exe': {'bundle_files': 1, 'compressed': True, 'dist_dir': build_path}},
    windows=[{'script': "steering_all.py", "icon_resources": [(1, "assets/icon/icon.ico")]}],

    zipfile=None,
)
