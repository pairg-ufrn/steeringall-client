import pygame
from copy import deepcopy

from sta.definitions import GameDefinitions


class DotScreen(object):

    def __init__(self, screen_width, screen_height, fps=60, screen_mode=0):
        self.width = screen_width
        self.height = screen_height
        self.fps = fps
        self.on = True
        self.milis = 0

        window_dim = (int(screen_width), int(screen_height))
        pygame.display.set_caption('Steering All')
        self.displaysurf = pygame.display.set_mode(window_dim, screen_mode)
        self.clock = pygame.time.Clock()
        GameDefinitions.CLOCK = self.clock

        self.world = None
        self.fill_color = GameDefinitions.FILL_COLOR

        self.interacts = []
        self.evt_interacts = []
        self.smlite = SmLite()

    def start_interacts(self):
        for interact in self.interacts:
            interact.startup()

        for evt_interact in self.evt_interacts:
            evt_interact.startup()

    def halt_interacts(self):
        for interact in self.interacts:
            interact.halt()

        for evt_interact in self.evt_interacts:
            evt_interact.halt()

    def notify_interacts(self):
        for interact in self.interacts:
            interact.check_interaction()

        events = pygame.event.get()
        for evt_interact in self.evt_interacts:
            evt_interact.check_event_interaction(events)

    def add_interact(self, interact):
        self.interacts.append(interact)

    def add_event_interact(self, interact):
        self.evt_interacts.append(interact)

    def set_all_interacts_listener(self, listener):
        for interact in self.interacts:
            interact.set_listener(listener)

        for interact in self.evt_interacts:
            interact.set_listener(listener)

    def set_world(self, world_id, args={}):
        world = deepcopy(self.smlite.get_state(world_id))
        world.screen = self
        world.init_args = args
        world.on_set_screen()
        self.set_all_interacts_listener(world)
        self.world = world

    def turn_off(self):
        self.on = False

    def step(self):
        GameDefinitions.set_frame_speed()

        self.notify_interacts()
        self.milis = self.clock.tick(self.fps)
        self.displaysurf.fill(self.fill_color)

        if self.world is not None:
            self.world.update()

        pygame.display.update()


class DynamicEntity(object):

    def __init__(self):
        self.values = {}

    def get_value(self, value_id):
        if value_id in self.values:
            return self.values[value_id]
        else:
            return None

    def set_value(self, value_id, value, ignore_on_id=False):
        self.values.update({value_id: value})
        if ignore_on_id:
            self.id_ignored_names.append(value_id)


class GameEntity(DynamicEntity):

    def __init__(self):
        self.parent = None
        self.marked_for_dispose = False
        super(GameEntity, self).__init__()

    def dispose(self):
        self.marked_for_dispose = True

    def handle_input(self, game_input):
        pass

    def step(self):
        pass

    def draw(self, displaysurf):
        pass


class GameComponent(GameEntity):

    def __init__(self):
        self.x = 0
        self.y = 0
        self.margin = [0, 0, 0, 0]
        super(GameComponent, self).__init__()

    def get_width(self):
        return 0

    def get_height(self):
        return 0


class GameComponentCollection(GameComponent):

    def __init__(self):
        self.children = []
        super(GameComponentCollection, self).__init__()

    def add_child(self, child):
        child.parent = self
        self.children.append(child)

        return child

    def remove_child(self, child):
        self.children.remove(child)

    def handle_input(self, game_input):
        for child in self.children:
            child.handle_input(game_input)

    def step(self):
        to_be_removed = []
        for child in self.children:
            child.step()
            if child.marked_for_dispose:
                to_be_removed.append(child)

        for child in to_be_removed:
            self.remove_child(child)

    def draw(self, displaysurf):
        for child in self.children:
            child.draw(displaysurf)


class SmLite(object):

    def __init__(self):
        self.states = {}

    def add_state(self, state_name, state):
        self.states[state_name] = state

    def get_state(self, state_name):
        return self.states[state_name]
