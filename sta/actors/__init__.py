from decimal import *

from sta import GameComponent
from sta.definitions import GameDefinitions
from sta.definitions.commands import GameCommands
from sta.storages import SoundStorage


class GameDescriptor(GameComponent):

    def __init__(self):
        self.level = 1
        self.score = 0
        self.distance = 0
        self.coins = 0
        self.max_lives = 3
        self.lives = self.max_lives

        self.is_paused = False

        self.march = 0
        self.is_boosting = False
        self.is_breaking = False
        self.boost_time = 0
        super(GameDescriptor, self).__init__()

    def reset_speed(self):
        self.march = 0
        self.is_boosting = False
        self.is_breaking = False
        self.boost_time = 0

    def handle_input(self, game_input):
        if game_input == GameCommands.BOOST:
            self.is_boosting = True

        if game_input == GameCommands.BOOST_RELEASE:
            self.is_boosting = False

        key_breaking = (game_input == GameCommands.BREAK)
        if key_breaking and not self.is_boosting:
            self.is_breaking = True

        key_unbreaking = (game_input == GameCommands.BREAK_RELEASE)
        if key_unbreaking and self.is_breaking:
            self.is_breaking = False

    def step(self):
        step_rate = Decimal(1) / GameDefinitions.get_current_fps()
        if not self.is_boosting and not self.is_breaking:
            if self.boost_time > 0:
                self.boost_time -= step_rate / 2

            if self.boost_time < 1:
                self.boost_time = 0

        elif self.is_boosting:

            if self.boost_time == 0:
                self.boost_time = 1

            if self.boost_time < 4:
                self.boost_time += step_rate * 2

        elif self.is_breaking:

            if self.boost_time > 0:
                self.boost_time -= step_rate * 8

            if self.boost_time < 1:
                self.boost_time = 0

        self.march = int(self.boost_time)


class SoundEntity(object):

    def play_sound(self, loops=0, maxtime=0, fade_ms=0):
        if not GameDefinitions.SOUND_ENABLED:
            return

        if self.get_value('sound') is None:
            sound = SoundStorage.get_sound(self.sound_path)
            self.set_value('sound', sound)
            sound.set_volume(self.sound_volume)

        sound = self.get_value('sound')
        sound.play(loops, maxtime, fade_ms)

    def stop_sound(self):
        if not GameDefinitions.SOUND_ENABLED:
            return
        if self.get_value('sound') is not None:
            self.get_value('sound').stop()
