from sta.grid import Grid


class AreaGrid(Grid):

    def __init__(self, color, width, height):
        self.color = color
        self.width = width
        self.height = height
        super(AreaGrid, self).__init__()

    def get_resources(self):
        return [self.color]

    def get_grid_definition(self):
        grid_def = []
        for i in range(0, self.height):
            line = []
            for j in range(0, self.width):
                line.append(1)

            grid_def.append(line)

        return grid_def
