from sta.actors.areas import AreaGrid
from sta.behaviors import SingleCollideBehavior
from sta.bodies import GridGameBody
from sta.interacts import GameControllerWrapper


class InvertedArea(GridGameBody):

    def __init__(self, width, height):
        super(InvertedArea, self).__init__(AreaGrid('splash-brick.png', width, height))


class InvertedAreaBehavior(SingleCollideBehavior):

    def on_collide(self, body):
        GameControllerWrapper.inverted = True

    def on_uncollide(self, body):
        GameControllerWrapper.inverted = False
