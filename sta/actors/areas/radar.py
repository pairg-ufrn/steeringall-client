from sta.bodies import GridGameBody

from . import AreaGrid


class DotRadar(GridGameBody):

    def __init__(self, width, height):
        super(DotRadar, self).__init__(
            AreaGrid('radar-brick.png', width, height))
