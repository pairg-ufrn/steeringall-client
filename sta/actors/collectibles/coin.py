from sta.bodies import GridGameBody
from sta.grid import Grid

from sta.behaviors import SingleCollideBehavior
from sta.storages import SoundStorage

class Coin(GridGameBody):

    def __init__(self):
        super(Coin, self).__init__(CoinGrid())


class CoinGrid(Grid):

    def get_resources(self):
        return ['coin-brick.png']

    def get_grid_definition(self):
        return [
            [1]
        ]


class CoinBehavior(SingleCollideBehavior):

    def __init__(self, target, game_descriptor):
        self.game_descriptor = game_descriptor
        super(CoinBehavior, self).__init__(target)

    def on_collide(self, body):
        SoundStorage.play_sound('assets/music/effects/levelup.wav')
        self.game_descriptor.score += 50
        self.game_descriptor.coins += 1
        body.dispose()
