from sta.bodies import GridGameBody
from sta.grid import Grid

from sta.behaviors import SingleCollideBehavior
from sta.storages import SoundStorage

class Shield(GridGameBody):

    def __init__(self):
        super(Shield, self).__init__(ShieldGrid())


class ShieldGrid(Grid):

    def get_resources(self):
        return ['shield-brick.png']

    def get_grid_definition(self):
        return [
            [1]
        ]


class ShieldBehavior(SingleCollideBehavior):

    def __init__(self, target, game_descriptor):
        self.game_descriptor = game_descriptor
        super(ShieldBehavior, self).__init__(target)

    def on_collide(self, body):
        SoundStorage.play_sound('assets/music/effects/shield.wav')
        self.game_descriptor.get_value('invencible_observer').make_invencible()
        body.dispose()
