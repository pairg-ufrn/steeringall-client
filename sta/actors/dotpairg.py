from sta.behaviors.draw import DrawAlphaBehavior
from sta.bodies import GridGameBody
from sta.grid import Grid


class DotPairg(GridGameBody):

    def __init__(self):
        super(DotPairg, self).__init__(PairgGrid(), scale=0.9)
        self.add_behavior(DrawAlphaBehavior())


class PairgGrid(Grid):

    def __init__(self):
        super(PairgGrid, self).__init__()
        self.theme_enabled = False

    def get_resources(self):
        return [
            "assets/img/plain/red-plain.png",
            "assets/img/plain/orange-plain.png",
            "assets/img/plain/green-plain.png",
            "assets/img/plain/blue-plain.png",
            "assets/img/plain/black-plain.png",
            "assets/img/plain/gray-plain.png"
        ]

    def get_grid_definition(self):
        return [
            [0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 6, 6, 6, 6, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 1, 1, 1, 1, 6, 6, 6, 6, 6, 6, 6, 6, 2, 2, 2, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 1, 1, 6, 6, 2, 2, 2, 2, 2, 2, 2, 2, 6, 6, 2, 2, 0, 0, 0, 0],
            [0, 0, 1, 1, 6, 6, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 6, 6, 2, 0, 0, 0],
            [0, 1, 1, 1, 6, 2, 2, 2, 2, 2, 2, 2, 2, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 0],
            [0, 1, 1, 6, 2, 2, 2, 2, 2, 2, 6, 6, 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 6, 0],
            [1, 1, 6, 6, 2, 2, 2, 2, 6, 6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 6, 4, 0],
            [1, 6, 1, 6, 2, 2, 2, 6, 3, 3, 3, 3, 3, 6, 6, 6, 6, 6, 6, 6, 3, 6, 4, 0],
            [6, 1, 1, 6, 2, 2, 6, 3, 3, 3, 6, 6, 6, 3, 3, 3, 3, 6, 4, 4, 6, 6, 4, 0],
            [6, 1, 1, 6, 2, 6, 3, 3, 3, 6, 3, 3, 3, 3, 3, 3, 3, 3, 6, 4, 4, 6, 4, 4],
            [1, 1, 1, 6, 6, 3, 3, 3, 6, 3, 3, 3, 3, 3, 3, 0, 0, 0, 6, 4, 4, 6, 4, 4],
            [0, 1, 1, 6, 6, 3, 3, 6, 3, 3, 3, 3, 0, 0, 0, 0, 0, 5, 5, 6, 4, 4, 6, 4],
            [0, 0, 1, 6, 3, 3, 6, 3, 3, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 6, 4, 4, 6, 4],
            [0, 0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 5, 5, 6, 4, 6, 4, 4],
            [0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 4, 6, 4, 4, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 5, 5, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 6, 5, 5, 5, 5, 5, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 5, 5, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 0, 0, 0, 0]
        ]
