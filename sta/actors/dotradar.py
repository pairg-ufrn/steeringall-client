from sta.actors.areas import AreaGrid
from sta.actors.vehicles import OnHeroCollideCarBehavior
from sta.bodies import GridGameBody


class DotRadar(GridGameBody):

    def __init__(self, width):
        super(DotRadar, self).__init__(AreaGrid('radar-brick.png', width, 1))


class RadarBehavior(OnHeroCollideCarBehavior):

    def __init__(self, target, game_descriptor, world, road):
        self.road = road
        self.is_hittable = False
        self.limit_march = 3
        super(RadarBehavior, self).__init__(target, game_descriptor, world)

    def do_step(self, body):
        if self.game_descriptor.march < self.limit_march and self.is_hittable:
            self.is_hittable = False
            body.set_value('alpha', 100)

        if self.game_descriptor.march >= self.limit_march and not self.is_hittable:
            self.is_hittable = True
            body.set_value('alpha', 255)

        if self.is_hittable:
            super(RadarBehavior, self).do_step(body)

