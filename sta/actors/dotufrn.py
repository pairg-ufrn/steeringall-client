from sta.behaviors.draw import DrawAlphaBehavior
from sta.bodies import GridGameBody
from sta.grid import Grid


class DotUFRN(GridGameBody):

    def __init__(self):
        super(DotUFRN, self).__init__(UFRNGrid(), scale=0.9)
        self.add_behavior(DrawAlphaBehavior())


class UFRNGrid(Grid):

    def get_resources(self):
        return [
            "color/blue-brick.png",
            "color/dark-blue-brick.png"
        ]

    def get_grid_definition(self):
        return [
            [0, 0, 0, 0, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1],
            [1, 0, 0, 1, 0, 2, 2, 2, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1],
            [1, 0, 0, 1, 0, 0, 0, 2, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1],
            [1, 0, 0, 1, 0, 2, 2, 2, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1],
            [1, 1, 1, 1, 0, 2, 2, 2, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ]
