from sta import GameEntity
from sta.definitions import dotget
from sta.utils.time import Timer, DistanceMarker

class DistanceScoreObserver(GameEntity):

    def __init__(self, game_descriptor):
        self.game_descriptor = game_descriptor
        self.marker = DistanceMarker(game_descriptor)
        self.distance_to_point = dotget(1)
        super(DistanceScoreObserver, self).__init__()

    def step(self):
        self.marker.step()
        if self.marker.distance_counter > self.distance_to_point:
            self.marker.loop(self.distance_to_point)
            self.game_descriptor.score += 1
            self.game_descriptor.distance += 1


class LevelObserver(GameEntity):

    def __init__(self, game_descriptor):
        self.game_descriptor = game_descriptor
        self.points_to_level = self.get_points_progression()
        super(LevelObserver, self).__init__()

    def get_points_progression(self):
        return self.game_descriptor.level * 100

    def step(self):
        if self.game_descriptor.distance > self.points_to_level:
            self.game_descriptor.level += 1
            self.points_to_level = self.get_points_progression()


class InvencibleObserver(GameEntity):

    def __init__(self, shield):
        self.shield = shield
        self.is_invencible = False
        self.fade_time = 0
        self.invencible_timer = Timer()
        super(InvencibleObserver, self).__init__()

    def make_invencible(self):
        self.is_invencible = True
        self.shield.visible = True
        self.shield.set_value('alpha', 255)
        self.invencible_timer.reset()

    def step(self):
        if self.is_invencible:
            self.invencible_timer.step()
            self.check_invencible_time()

    def check_invencible_time(self):
        if self.invencible_timer.counter > 6:
            self.disable_invencibility()

        if self.invencible_timer.counter > 2:
            c = int(self.invencible_timer.counter)
            if c % 2 == 0:
                self.shield.set_value('alpha', 200)
            else:
                self.shield.set_value('alpha', 140)

    def disable_invencibility(self):
        self.is_invencible = False
        self.shield.visible = False
        self.invencible_timer.reset()
