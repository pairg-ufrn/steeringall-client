from sta import GameComponentCollection
from sta.bodies import BodyPositioner
from sta.definitions import dottextsize
from sta.utils.text import DotText


class DotScoreAndLevelHUD(GameComponentCollection):

    def __init__(self, game_descriptor, scale=1):
        super(DotScoreAndLevelHUD, self).__init__()
        self.game_descriptor = game_descriptor
        self.scale = scale
        self.score_label = self.add_child(DotText(text="Score: 0"))
        self.distance_label = self.add_child(DotText(text="Distance: 0 m"))
        self.coins_label = self.add_child(DotText(text="Coins: 0"))
        self.level_label = self.add_child(DotText(text="Level: 0"))


    def get_height(self):
        self.position()
        return self.score_label.get_height() + (self.level_label.get_height() * 3)


    def position(self):
        self.score_label.x = self.x
        self.score_label.y = self.y

        self.distance_label.x = self.x
        BodyPositioner.below(self.distance_label, self.score_label)

        self.coins_label.x = self.x
        BodyPositioner.below(self.coins_label, self.distance_label)

        self.level_label.x = self.x
        BodyPositioner.below(self.level_label, self.coins_label)

        self.score_label.set_size(dottextsize(24))
        self.distance_label.set_size(dottextsize(24))
        self.coins_label.set_size(dottextsize(24))
        self.level_label.set_size(dottextsize(24))

        self.score_label.font = "assets/fonts/FiraMono-Regular.ttf"
        self.distance_label.font = "assets/fonts/FiraMono-Regular.ttf"
        self.coins_label.font = "assets/fonts/FiraMono-Regular.ttf"
        self.level_label.font = "assets/fonts/FiraMono-Regular.ttf"

        self.score_label.wrap()
        self.distance_label.wrap()
        self.coins_label.wrap()
        self.level_label.wrap()

    def step(self):
        self.score_label.text = "Score: " + str(self.game_descriptor.score) + " pts"
        self.distance_label.text = "Distance: " + str(self.game_descriptor.distance) + " m"
        self.coins_label.text = "Coins: " + str(self.game_descriptor.coins)
        self.level_label.text = "Level: " + str(self.game_descriptor.level)
        self.position()
