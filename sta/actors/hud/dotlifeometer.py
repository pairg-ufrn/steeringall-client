from sta import GameComponentCollection
from sta.bodies import GridGameBody, BodyPositioner
from sta.definitions import dotget
from sta.grid import GenericGrid


class DotLifeOMeter(GameComponentCollection):

    def __init__(self, game_descriptor, scale=1):
        super(DotLifeOMeter, self).__init__()
        self.positioned = False
        self.current_lives = 0
        self.game_descriptor = game_descriptor
        grid = [
            [1]
        ]
        self.scale = scale

        for i in range(0, self.game_descriptor.max_lives):
            self.add_child(
                DotLifeMarker(['heart.png'], self.scale))

    def position_children(self):
        difference = self.current_lives - self.game_descriptor.lives
        # We lost lives
        if difference == 0:
            return

        self.current_lives = self.game_descriptor.lives
        for i, child in enumerate(self.children):
            if i < self.game_descriptor.lives:
                child.grid.resource_list = ['heart.png']
            else:
                child.grid.resource_list = ['empty-heart.png']
                child.set_value('empty', True)

            child.surface = child.build_grid_surface()


        self.positioned = True
        for index, child in enumerate(self.children):
            child.x = self.x
            if index == 0:
                child.y = self.y - child.get_height()
            else:
                BodyPositioner.margin_bottom(
                    child, dotget(float(self.scale) / 4))
                BodyPositioner.above(child, previous_child)

            previous_child = child

    def step(self):
        self.position_children()


class DotLifeMarker(GridGameBody):

    def __init__(self, res, scale=1):
        grid = [
            [1]
        ]
        super(DotLifeMarker, self).__init__(GenericGrid(res, grid), scale)
