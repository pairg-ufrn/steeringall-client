from sta import GameComponentCollection
from sta.bodies import GridGameBody, BodyPositioner
from sta.definitions import dotget
from sta.grid import GenericGrid


class DotSpeedOMeter(GameComponentCollection):

    def __init__(self, game_descriptor, scale=1):
        super(DotSpeedOMeter, self).__init__()
        self.positioned = False
        self.current_march = -1
        self.game_descriptor = game_descriptor
        grid = [
            [1]
        ]
        self.scale = scale

        self.add_child(
            DotSpeedMarker('speed/1-brick.png', grid, self.scale))
        self.add_child(
            DotSpeedMarker('speed/2-brick.png', grid, self.scale))
        self.add_child(
            DotSpeedMarker('speed/3-brick.png', grid, self.scale))
        self.add_child(
            DotSpeedMarker('speed/4-brick.png', grid, self.scale))
        self.add_child(
            DotSpeedMarker('speed/5-brick.png', grid, self.scale))

    def position_children(self):
        if self.positioned:
            return

        self.positioned = True
        previous_child = None
        for child in self.children:
            child.x = self.x
            if previous_child is None:
                child.y = self.y - child.get_height()
            else:
                BodyPositioner.margin_bottom(
                    child, dotget(float(self.scale) / 4))
                BodyPositioner.above(child, previous_child)

            previous_child = child

    def step(self):
        self.position_children()
        if self.current_march != self.game_descriptor.march:
            self.current_march = self.game_descriptor.march
            for index, child in enumerate(self.children):
                if index > self.game_descriptor.march:
                    child.visible = False
                else:
                    child.visible = True


class DotEmbedSpeedOMeter(GameComponentCollection):

    def __init__(self, game_descriptor, hero, scale=1):
        super(DotEmbedSpeedOMeter, self).__init__()
        self.hero = hero
        self.positioned = False
        self.current_march = -1
        self.game_descriptor = game_descriptor
        grid = [
            [1]
        ]
        self.scale = scale

        self.add_child(
            DotSpeedMarker(['speed/1-brick.png'], grid, self.scale))
        self.add_child(
            DotSpeedMarker(['speed/2-brick.png'], grid, self.scale))
        self.add_child(
            DotSpeedMarker(['speed/3-brick.png'], grid, self.scale))
        self.add_child(
            DotSpeedMarker(['speed/4-brick.png'], grid, self.scale))

        hero_space = (self.hero.get_height() / 4) * 3
        space = hero_space - self.get_child_height()
        self.border = float(space) / len(self.children)
        # self.border = 0

    def get_width(self):
        if len(self.children) == 0:
            return 0

        return self.children[0].get_width()

    def get_height(self):
        child_count = len(self.children)
        if child_count == 0:
            return 0
        return self.get_child_height() + (self.border * child_count)

    def get_child_height(self):
        child_count = len(self.children)
        if child_count == 0:
            return 0
        return self.children[0].get_height() * child_count

    def position_children(self):
        if self.positioned:
            return

        self.positioned = True
        previous_child = None
        for child in self.children:
            child.x = self.x
            if previous_child is None:
                child.y = self.y - child.get_height()
            else:
                BodyPositioner.margin_bottom(
                    child, self.border)
                BodyPositioner.above(child, previous_child)

            previous_child = child

    def step(self):
        BodyPositioner.center_relative_x(self, self.hero)
        self.y = self.hero.y + (self.hero.get_height() / 4) * 3

        for child in self.children:
            child.x = self.x

        self.position_children()
        if self.current_march != self.game_descriptor.march:
            self.current_march = self.game_descriptor.march
            for index, child in enumerate(self.children):
                if index > self.game_descriptor.march - 1:
                    child.visible = False
                else:
                    child.visible = True


class DotSpeedMarker(GridGameBody):

    def __init__(self, speed_res, grid, scale=1):
        super(DotSpeedMarker, self).__init__(
            GenericGrid([speed_res], grid), scale)
        self.set_value('spd', speed_res)
