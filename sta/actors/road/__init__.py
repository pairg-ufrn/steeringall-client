from sta.actors.vehicles import DotCar
from sta.behaviors.draw import DrawAlphaBehavior
from sta.bodies import GridGameBody
from sta.definitions import GameDefinitions
from sta.grid import Grid
from .border import DotBorder


class DotRoad(GridGameBody):

    def __init__(self, trails):
        self.trail_count = trails
        super(DotRoad, self).__init__(TrailGrid(trails))
        self.surface = self.build_grid_surface()
        self.add_behavior(DrawAlphaBehavior())
        self.set_value('alpha', 55)
        self.border_width = DotBorder().get_width()
        self.trail_width = DotCar().get_width()
        self.set_value('cache_enabled', False)

    def get_border_width(self):
        return self.border_width

    def get_trail_width(self):
        return self.trail_width


class TrailGrid(Grid):

    def __init__(self, trails):
        self.trails = trails
        super(TrailGrid, self).__init__()

    def get_resources(self):
        return ["ground-brick.png"]

    def get_grid_definition(self):
        return self.build_grid()

    def build_grid(self):
        grid = []
        traildraw = (self.trails * 3) + 2
        for row in range(0, GameDefinitions.WINDOW_HEIGHT):
            element = []
            for column in range(0, traildraw):
                element.append(1)

            grid.append(element)

        return grid
