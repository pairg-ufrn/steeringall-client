from sta.bodies import GridGameBody
from sta.grid import Grid


class DotBorder(GridGameBody):

    def __init__(self):
        super(DotBorder, self).__init__(BorderGrid())


class BorderGrid(Grid):

    def get_resources(self):
        return ['border-brick.png']

    def get_grid_definition(self):
        return [
            [1],
            [1]
        ]
