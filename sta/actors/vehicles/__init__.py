from sta.behaviors import SingleCollideBehavior, GameBehavior
from sta.bodies import GridGameBody
from sta.grid import Grid
from sta.storages import SoundStorage
from sta.utils.rand import RandomPercentage


class DotCar(GridGameBody):

    def __init__(self):
        color = self.get_random_color()
        super(DotCar, self).__init__(CarGrid(color))
        self.set_value('color', color)

    def get_random_color(self):
        alt_chance = RandomPercentage(40)
        if alt_chance.is_successful():
            return "default-car-brick.png"
        else:
            return "alt-car-brick.png"


class CarGrid(Grid):

    def __init__(self, color):
        self.color = color
        super(CarGrid, self).__init__()

    def get_resources(self):
        return [self.color]

    def get_grid_definition(self):
        return [
            [0, 1, 0],
            [1, 1, 1],
            [0, 1, 0],
            [1, 0, 1]
        ]


class CarBehavior(GameBehavior):

    def __init__(self, trail, start_trail=None):
        super(CarBehavior, self).__init__()
        self.trail = trail
        self.start_trail = start_trail

    def init_behavior(self, body):
        if self.start_trail is None:
            self.start_trail = int(self.trail.trail_count / 2)
        body.set_value('road', self.start_trail, ignore_on_id=True)

    def do_step(self, body):
        trail = int(body.get_value('road'))
        base_x = self.trail.x + self.trail.get_border_width()
        trails_width = self.trail.get_trail_width()
        body.x = base_x + (trail * trails_width)


class OnHeroCollideCarBehavior(SingleCollideBehavior):

    def __init__(self, target, game_descriptor, world):
        self.world = world
        self.game_descriptor = game_descriptor
        super(OnHeroCollideCarBehavior, self).__init__(target)

    def on_collide(self, body):
        invencible_observer = self.game_descriptor.get_value('invencible_observer')
        if invencible_observer.is_invencible:
            return

        SoundStorage.play_sound('assets/music/effects/collide.wav')
        self.game_descriptor.lives -= 1
        self.game_descriptor.reset_speed()
        self.world.on_collide()
        self.world.pausable = False
        body.set_value('alpha', 255)
        body.visible = True

        self.target.grid = XGrid()
        self.target.surface = self.target.build_grid_surface()


class XGrid(Grid):

    def __init__(self):
        super(XGrid, self).__init__()

    def get_resources(self):
        return ['hero-crash-brick.png']

    def get_grid_definition(self):
        return [
            [1, 0, 1],
            [0, 1, 0],
            [1, 0, 1]
        ]
