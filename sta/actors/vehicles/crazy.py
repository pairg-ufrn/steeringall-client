from decimal import Decimal

from sta.actors import SoundEntity
from sta.behaviors import GameBehavior
from sta.bodies import GridGameBody
from sta.utils.time import Timer
from . import CarGrid


class DotCrazy(GridGameBody, SoundEntity):

    def __init__(self):
        super(DotCrazy, self).__init__(CarGrid('crazy-brick.png'))
        self.sound_path = 'assets/music/effects/crazy_collide.wav'
        self.sound_volume = 0.8


class CrazyBehavior(GameBehavior):

    GOING_LEFT = 0
    GOING_RIGHT = 1

    def __init__(self, game_descriptor, trail):
        self.game_descriptor = game_descriptor
        self.timer = Timer()
        self.trail = trail
        super(CrazyBehavior, self).__init__()

    def init_behavior(self, body):
        body.set_value('direction', CrazyBehavior.GOING_LEFT, True)

    def do_step(self, body):
        super(CrazyBehavior, self).do_step(body)
        level = self.game_descriptor.level
        self.timer.step()
        limit = Decimal(0.5)
        if self.timer.counter > limit:
            self.timer.loop(limit)
            self.go_crazy(body)

    def go_crazy(self, body):
        going_left = body.get_value('direction') == CrazyBehavior.GOING_LEFT
        going_right = body.get_value('direction') == CrazyBehavior.GOING_RIGHT
        trail = body.get_value('road')

        if going_left and trail > 0:
            trail -= 1
            body.set_value('road', trail)
            if trail == 0:
                body.set_value('just_changed', True)
                body.set_value('direction', CrazyBehavior.GOING_RIGHT)
                return

        if going_right and trail < self.trail.trail_count - 1:
            trail += 1
            body.set_value('road', trail)
            if trail == self.trail.trail_count - 1:
                body.set_value('just_changed', True)
                body.set_value('direction', CrazyBehavior.GOING_LEFT)
                return

        if going_right and trail == self.trail.trail_count - 1:
            body.set_value('just_changed', True)
            body.set_value('direction', CrazyBehavior.GOING_LEFT)
            body.set_value('road', trail - 1)

        if going_left and trail == 0:
            body.set_value('just_changed', True)
            body.set_value('direction', CrazyBehavior.GOING_RIGHT)
            body.set_value('road', trail + 1)

        just_changed = body.get_value('just_changed')
        if just_changed is True:
            body.play_sound(loops=0)
            body.set_value('just_changed', False)
