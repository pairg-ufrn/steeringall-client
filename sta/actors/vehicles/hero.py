from sta.bodies import GridGameBody
from sta.behaviors import GameBehavior
from sta.definitions import dotget
from sta.definitions.commands import GameCommands
from . import CarGrid, CarBehavior

from sta.grid import Grid


class DotHero(GridGameBody):

    def __init__(self):
        super(DotHero, self).__init__(CarGrid('hero-brick.png'))
        self.set_value('cache_enabled', False)

    def get_hit_dimensions(self):
        width = self.get_width()
        height = self.get_height() - dotget(2, horizontal=False)
        return width, height

    def get_hit_coordinates(self):
        return self.x, self.y + dotget(1, horizontal=False)


class HeroBehavior(CarBehavior):

    def do_handle_input(self, body, game_input):
        trail = body.get_value('road')
        left_allowed = (trail > 0)

        if game_input == GameCommands.LEFT and left_allowed:
            body.set_value('road', trail - 1)

        right_allowed = (trail < (self.trail.trail_count - 1))
        if game_input == GameCommands.RIGHT and right_allowed:
            body.set_value('road', trail + 1)


class DotShield(GridGameBody):

    def __init__(self):
        super(DotShield, self).__init__(DotShieldGrid())


class DotShieldGrid(Grid):

    def get_resources(self):
        return [
            'full-shield-brick.png',
            'half-left-shield-brick.png',
            'half-right-shield-brick.png'
        ]

    def get_grid_definition(self):
        return [
            [2, 1, 3],
            [1, 0, 1]
        ]

class AddonBehavior(GameBehavior):

    def do_step(self, body):
        body.x = body.get_value('target').x + body.get_value('x_offset')
        body.y = body.get_value('target').y + body.get_value('y_offset')
