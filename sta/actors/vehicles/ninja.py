from sta.behaviors import GameBehavior
from sta.bodies import GridGameBody
from sta.definitions import GameDefinitions
from . import CarGrid


class DotNinja(GridGameBody):

    def __init__(self):
        super(DotNinja, self).__init__(CarGrid('ninja-brick.png'))


class NinjaBehavior(GameBehavior):

    def __init__(self):
        self.game_height = int(GameDefinitions.get_game_height() / 3)
        self.max_state = 8
        super(NinjaBehavior, self).__init__()

    def init_behavior(self, body):
        body.set_value('hidden', False, ignore_on_id=True)
        body.set_value('current_state', 0, ignore_on_id=True)
        body.set_value('old_state', 0, ignore_on_id=True)
        body.set_value('cache_enabled', False, ignore_on_id=True)

    def do_step(self, body):
        hidden = body.get_value('hidden')
        if hidden:
            body.set_value('alpha', 0)
            return

        state = body.get_value('current_state')
        if state > self.max_state:
            body.set_value('hidden', True)
            body.set_value('alpha', 0)
            return

        if state != body.get_value('old_state'):
            divisor = 1 + state
            alpha = int(255) / divisor
            body.set_value('alpha', alpha)

        adjusted_y = body.y + body.get_height()
        state_limit = (self.game_height / self.max_state) * (1 + state)

        if adjusted_y > state_limit:
            body.set_value('current_state', state + 1)

