from decimal import Decimal

from sta.actors import SoundEntity
from sta.behaviors import GameBehavior
from sta.bodies import GridGameBody
from sta.grid import Grid
from sta.utils.time import Timer


class DotPolice(GridGameBody, SoundEntity):

    def __init__(self):
        super(DotPolice, self).__init__(PoliceGrid())
        self.sound_path = 'assets/music/effects/police_move.wav'
        self.sound_volume = 0.8


class PoliceGrid(Grid):

    def get_grid_definition(self):
        return [
            [0, 1, 0],
            [1, 1, 1],
            [0, 1, 0],
            [1, 0, 1]
        ]

    def get_resources(self):
        return ["police-brick.png"]


class PoliceBehavior(GameBehavior):

    def __init__(self, game_descriptor, hero, trail):
        self.game_descriptor = game_descriptor
        self.timer = Timer()
        self.trail = trail
        self.hero = hero
        super(PoliceBehavior, self).__init__()

    def do_step(self, body):
        super(PoliceBehavior, self).do_step(body)
        level = self.game_descriptor.level
        self.timer.step()
        limit = Decimal(1)
        if self.timer.counter > limit:
            self.timer.loop(limit)
            self.chase(body)

    def chase(self, body):
        hero_trail = self.hero.get_value('road')
        self_trail = body.get_value('road')
        if hero_trail > self_trail:
            body.play_sound()
            body.set_value('road', self_trail + 1)
        elif hero_trail < self_trail:
            body.play_sound()
            body.set_value('road', self_trail - 1)
