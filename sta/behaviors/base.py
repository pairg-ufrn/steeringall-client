class GameBehavior(object):

    def __init__(self):
        self.behavior_id = self.__class__.__name__
        self.is_blocking = False

    def init_behavior(self, body):
        pass

    def do_draw(self, body):
        pass

    def do_step(self, body):
        pass

    def do_handle_input(self, body, game_input):
        pass
