import pygame

from sta.storages import SurfaceStorage
from .base import GameBehavior


class DrawAlphaBehavior(GameBehavior):

    def init_behavior(self, body):
        body.set_value('alpha', 255)
        body.set_value('old_alpha', 255)
        body.set_value('master_surface', body.surface, ignore_on_id=True)
        body.set_value('cache_enabled', False)

    def do_draw(self, body):
        body_alpha = body.get_value('alpha')
        if body_alpha != body.get_value('old_alpha'):
            del body.surface
            body.surface = body.get_value('master_surface').copy()
            body.surface.fill(
                (255, 255, 255, body_alpha), None, pygame.BLEND_RGBA_MULT)
            body.set_value('old_alpha', body_alpha)


class DrawOpaqueAlphaBehavior(GameBehavior):

    def init_behavior(self, body):
        body.set_value('alpha', 255)

    def do_draw(self, body):
        body.surface.set_alpha(body.get_value('alpha'))


class DrawFromCacheBehavior(GameBehavior):

    def init_behavior(self, body):
        body.set_value('cache_enabled', True, ignore_on_id=True)
        body.set_value('loaded_from_cache', False, ignore_on_id=True)
        body.set_value('resource_id', body.__class__.__name__)
        body.set_value('resource_item', 'main')
        body.set_value('last_surface', None, ignore_on_id=True)

    def do_draw(self, body):
        # print len(SurfaceStorage.res_library)
        if body.get_value('cache_enabled'):
            body_id = body.get_obj_identifier()
            # print body_id
            surface = SurfaceStorage.get_res(body_id)
            if surface is not None:
                body.surface = surface
                body.set_value('loaded_from_cache', True)
                return True
            else:
                body.set_value('loaded_from_cache', False)

            loaded_from_cache = body.get_value('loaded_from_cache')
            has_surface = body.surface is not None
            not_cached = surface is None

            changed_surface = body.surface != body.get_value('last_surface')
            unique_surface = changed_surface and not_cached

            if has_surface and not loaded_from_cache and unique_surface:
                SurfaceStorage.set_res(body_id, body.surface)

            body.set_value('last_surface', body.surface)
