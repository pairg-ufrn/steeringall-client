import pygame

from sta.definitions import GameDefinitions, get_game_speed
from sta.storages import SoundStorage
from .base import GameBehavior


class FallStepBehavior(GameBehavior):

    def __init__(self, game_descriptor, modifier=1, fixed=False):
        self.game_descriptor = game_descriptor
        self.modifier = modifier
        self.fixed = fixed
        super(FallStepBehavior, self).__init__()

    def init_behavior(self, body):
        body.set_value('speed_modifier', self.modifier, ignore_on_id=True)

    def do_step(self, body):
        level = self.game_descriptor.level
        march = self.game_descriptor.march
        modifier = body.get_value('speed_modifier')

        y_speed = get_game_speed(level, march, modifier, fixed=self.fixed)

        body.y += y_speed
        if body.y > GameDefinitions.get_game_height():
            body.dispose()


class SingleCollideBehavior(GameBehavior):

    def __init__(self, target):
        super(SingleCollideBehavior, self).__init__()
        self.target = target
        self.is_colliding = False

    def do_step(self, body):
        targetrect = pygame.Rect(
            self.target.get_hit_coordinates(),
            self.target.get_hit_dimensions()
        )

        selfrect = pygame.Rect(
            body.get_hit_coordinates(),
            body.get_hit_dimensions()
        )

        actually_colliding = targetrect.colliderect(selfrect)
        if actually_colliding and not self.is_colliding:
            self.is_colliding = True
            self.on_collide(body)
        elif not actually_colliding and self.is_colliding:
            self.is_colliding = False
            self.on_uncollide(body)

    def on_collide(self, body):
        pass

    def on_uncollide(self, body):
        pass

