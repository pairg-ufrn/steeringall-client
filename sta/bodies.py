from sta import GameComponent
from sta.behaviors import DrawFromCacheBehavior
from sta.definitions import GameDefinitions
from sta.grid import GridBuilder


class GameBody(GameComponent):

    def __init__(self, scale=1):
        self.surface = None
        self.visible = True

        self.behaviors = []
        self.loaded_behaviors = []
        self.id_ignored_names = []
        super(GameBody, self).__init__()

        self.set_value('scale', scale)

    def add_behavior(self, behavior, force_load=False):
        self.behaviors.append(behavior)
        if force_load or behavior.behavior_id not in self.loaded_behaviors:
            behavior.init_behavior(self)
            self.loaded_behaviors.append(behavior.behavior_id)

    def remove_behavior(self, behavior_id):
        for behavior in self.behaviors:
            if behavior.behavior_id == behavior_id:
                self.behaviors.remove(behavior)


    def get_behavior(self, behavior_id):
        for behavior in self.behaviors:
            if behavior.behavior_id == behavior_id:
                return behavior

        return None

    def generate_surface(self):
        pass

    def get_width(self):
        if self.surface is not None:
            return self.surface.get_width()
        return 0

    def get_height(self):
        if self.surface is not None:
            return self.surface.get_height()
        return 0

    def get_hit_coordinates(self):
        return self.get_coordinates()

    def get_hit_dimensions(self):
        return self.get_dimensions()

    def get_coordinates(self):
        return self.x, self.y

    def get_dimensions(self):
        return self.get_width(), self.get_height()

    def get_rect(self):
        return (self.x, self.y), (self.get_width(), self.get_height())

    def handle_input(self, game_input):
        for behavior in self.behaviors:
            behavior.do_handle_input(self, game_input)

    def step(self):
        for behavior in self.behaviors:
            behavior.do_step(self)

    def draw(self, displaysurf):
        if self.visible:
            for behavior in self.behaviors:
                result = behavior.do_draw(self)
                if result is True:
                    break
            displaysurf.blit(self.surface, (self.x, self.y))

    def get_obj_identifier(self):
        values = []
        for key, item in self.values.iteritems():
            if key in self.id_ignored_names:
                continue
            values.append(key + '_' + str(item))
        obj_query_id = '_'.join(values)

        return obj_query_id


class GridGameBody(GameBody):

    def __init__(self, grid, scale=1):
        self.grid = grid
        super(GridGameBody, self).__init__(scale)
        self.surface = self.build_grid_surface()
        self.add_behavior(DrawFromCacheBehavior())

    def build_grid_surface(self):
        my_scale = self.get_value('scale')
        return GridBuilder.create_surface(self.grid, scale=my_scale)

    def get_width(self):
        my_scale = self.get_value('scale')
        return self.grid.get_grid_width(my_scale)

    def get_height(self):
        my_scale = self.get_value('scale')
        return self.grid.get_grid_height(my_scale)


class BodyPositioner(object):

    @staticmethod
    def below(source_body, target_body):
        margin = source_body.margin[1]
        source_body.y = target_body.y + target_body.get_height() + margin

    @staticmethod
    def above(source_body, target_body):
        margin = source_body.margin[3]
        source_body.y = target_body.y - source_body.get_height() - margin

    @staticmethod
    def left_of(source_body, target_body):
        margin = source_body.margin[2]
        source_body.x = target_body.x - source_body.get_width() - margin

    @staticmethod
    def right_of(source_body, target_body):
        margin = source_body.margin[0]
        source_body.x = target_body.x + target_body.get_width() + margin

    @staticmethod
    def margin(source_body, margin):
        source_body.margin = margin

    @staticmethod
    def margin_left(source_body, margin):
        source_body.margin[0] = margin
        source_body.x += margin

    @staticmethod
    def margin_right(source_body, margin):
        source_body.margin[2] = margin
        source_body.x -= margin

    @staticmethod
    def margin_top(source_body, margin):
        source_body.margin[1] = margin
        source_body.y += margin

    @staticmethod
    def margin_bottom(source_body, margin):
        source_body.margin[3] = margin
        source_body.y -= margin

    @staticmethod
    def align_left(source_body):
        source_body.x = 0 + source_body.margin[0]

    @staticmethod
    def align_right(source_body):
        total_width = GameDefinitions.get_game_width()
        body_margin = source_body.get_width() - source_body.margin[2]
        source_body.x = total_width - body_margin

    @staticmethod
    def align_top(source_body):
        source_body.y = 0 + source_body.margin[1]

    @staticmethod
    def align_bottom(source_body):
        total_height = GameDefinitions.get_game_height()
        body_margin = source_body.get_height() + source_body.margin[3]
        source_body.y = total_height - body_margin

    @staticmethod
    def center_x(source_body):
        total_width = GameDefinitions.get_game_width()
        source_body.x = (total_width / 2) - (source_body.get_width() / 2)

    @staticmethod
    def center_y(source_body):
        total_height = GameDefinitions.get_game_height()
        source_body.y = (total_height / 2) - (source_body.get_height() / 2)

    @staticmethod
    def center_relative_x(source_body, target_body):
        target_half_width = (target_body.get_width() / 2)
        source_half_width = (source_body.get_width() / 2)
        source_body.x = target_body.x + target_half_width - source_half_width

    @staticmethod
    def center_relative_y(source_body, target_body):
        target_half_height = (target_body.get_height() / 2)
        source_half_height = (source_body.get_height() / 2)
        source_body.y = target_body.y + target_half_height - source_half_height
