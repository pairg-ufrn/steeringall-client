import os
import pygame
import sys
import traceback

from sta import DotScreen
from sta.definitions import GameDefinitions, dotget
from sta.utils.moduleloader import ModuleLoader
from sta.interacts import GameControllerWrapper


class SteeringAllBootManager(object):

    def __init__(self):
        self.screen = None

    @staticmethod
    def init_pygame():
        pygame.init()

    @staticmethod
    def init_game_controller_wrapper():
        GameControllerWrapper.init_wrapper()

    @staticmethod
    def center_window():
        os.environ['SDL_VIDEO_CENTERED'] = '1'

    @staticmethod
    def add_modules_to_path():
        sys.path.append(os.getcwd())
        sys.path.append(os.path.join(os.getcwd(), 'modules'))

    @staticmethod
    def load_user_configs():
        GameDefinitions.load_user_configs()

    def init_screen(self):
        window_width = dotget(GameDefinitions.WINDOW_WIDTH)
        window_height = dotget(GameDefinitions.WINDOW_HEIGHT, horizontal=False)
        game_fps = GameDefinitions.FPS
        screen_mode = GameDefinitions.SCREEN_MODE

        self.screen = DotScreen(
            window_width, window_height, game_fps, screen_mode)

        self.load_worlds()

    def load_worlds(self):
        from sta.worlds.logoworlds.pairgworld import PairgLogoWorld
        from sta.worlds.logoworlds.ufrnworld import UfrnLogoWorld
        from sta.worlds.menuworld import TitleWorld
        from sta.worlds.gameworld import GameWorld
        from sta.worlds.logoworlds.gameoverworld import GameOverWorld

        self.screen.smlite.add_state('pairg', PairgLogoWorld())
        self.screen.smlite.add_state('ufrn', UfrnLogoWorld())
        self.screen.smlite.add_state('title', TitleWorld())
        self.screen.smlite.add_state('game', GameWorld())
        self.screen.smlite.add_state('game-over', GameOverWorld())

    def load_modules(self):
        loader = ModuleLoader()
        interact_modules = loader.load_modules()
        for interact in interact_modules:
            if not interact.get_enabled():
                # print interact.get_name() + " is disabled"
                continue

            if interact.type == GameDefinitions.MODULES_NATURE_EVENT:
                # print interact.get_name() + " loaded as event interacts"
                self.screen.add_event_interact(interact)
            elif interact.type == GameDefinitions.MODULES_NATURE_INTERACT:
                # print interact.get_name() + " loaded as interacts"
                self.screen.add_interact(interact)
        self.screen.start_interacts()

    def set_first_world(self):
        self.screen.set_world('pairg')

    def enter_game_loop(self):
        try:
            while self.screen.on:
                self.screen.step()
        except Exception:
            traceback.print_exc()

        self.screen.halt_interacts()
        pygame.quit()

    def boot_game(self):
        self.init_pygame()
        self.init_game_controller_wrapper()
        self.center_window()
        self.add_modules_to_path()
        self.load_user_configs()
        self.init_screen()
        self.load_modules()
        self.set_first_world()
        self.enter_game_loop()
