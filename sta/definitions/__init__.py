import ConfigParser
import os
import pygame


class GameDefinitions:

    @staticmethod
    def load_user_configs():
        config = ConfigParser.RawConfigParser()
        config.read('config.cfg')

        is_fullscreen = config.getboolean('VIDEO', 'fullscreen')
        GameDefinitions.FPS = config.getint('GAME', 'framerate')
        GameDefinitions.THEME = config.get('VIDEO', 'theme')
        if is_fullscreen:
            GameDefinitions.SCREEN_MODE = pygame.FULLSCREEN
        else:
            GameDefinitions.SCREEN_MODE = pygame.DOUBLEBUF

        GameDefinitions.SHADOW_ENABLED = config.getboolean(
            'VIDEO', 'enable_shadows')

        # config.get('VIDEO', 'resolution')

        GameDefinitions.SOUND_ENABLED = config.getboolean(
            'AUDIO', 'enable_sound')

        resolution = config.get('VIDEO', 'resolution')
        if resolution == 'default':
            info = pygame.display.Info()
            if is_fullscreen:
                resolution_width = info.current_w
                resolution_height = info.current_h
            else:
                resolution_width = int((info.current_w / 100) * 95)
                resolution_height = int((info.current_h / 100) * 95)
        else:
            splitted = resolution.split('x')
            resolution_width = splitted[0]
            resolution_height = splitted[1]

        GameDefinitions.DOT_WIDTH = GameDefinitions.solve_resolution(
            int(resolution_width), int(GameDefinitions.WINDOW_WIDTH)
        )

        GameDefinitions.DOT_HEIGHT = GameDefinitions.solve_resolution(
            int(resolution_height), int(GameDefinitions.WINDOW_HEIGHT)
        )

    @staticmethod
    def solve_resolution(total_resolution, parts):
        return int(total_resolution / parts)

    def __init__(self):
        pass

    THEME = 'flat'
    DEFAULT_THEME = 'flat'
    SHADOW_ENABLED = True
    SOUND_ENABLED = True
    SCREEN_MODE = pygame.NOFRAME

    BASE_SPEED = 4
    FRAME_SPEED_H = -1
    FRAME_SPEED_W = -1
    FPS = 60
    CLOCK = None

    DOT_WIDTH = 16
    DOT_HEIGHT = 16
    GAME_TRAILS = 7

    # Window dimensions (in Dots)
    WINDOW_WIDTH = 54
    WINDOW_HEIGHT = 32

    SCORE_DECIMAL = 1000

    MODULES_DIR = os.path.abspath('./modules')
    MODULES_NATURE_EVENT = 'event'
    MODULES_NATURE_INTERACT = 'raw'

    SERVER_ADDRESS = 'localhost'
    SERVER_PORT = 8090

    FILL_COLOR = (248, 248, 248)

    @staticmethod
    def get_game_width():
        return dotget(GameDefinitions.WINDOW_WIDTH, horizontal=True)

    @staticmethod
    def get_game_height():
        return dotget(GameDefinitions.WINDOW_HEIGHT, horizontal=False)

    @staticmethod
    def get_game_speed(horizontal=False):
        fps = GameDefinitions.get_current_fps()
        if horizontal:
            block = GameDefinitions.DOT_WIDTH
        else:
            block = GameDefinitions.DOT_HEIGHT

        one_frame_block = float(block) / float(fps)

        return float(GameDefinitions.BASE_SPEED * one_frame_block)

    @staticmethod
    def set_frame_speed():
        GameDefinitions.FRAME_SPEED_W = GameDefinitions.get_game_speed(True)
        GameDefinitions.FRAME_SPEED_H = GameDefinitions.get_game_speed(False)

    @staticmethod
    def get_frame_speed(horizontal=False):
        if horizontal:
            if GameDefinitions.FRAME_SPEED_W == -1:
                GameDefinitions.set_frame_speed()

            return GameDefinitions.FRAME_SPEED_W
        else:
            if GameDefinitions.FRAME_SPEED_H == -1:
                GameDefinitions.set_frame_speed()

            return GameDefinitions.FRAME_SPEED_H

    @staticmethod
    def get_current_fps():
        fps = GameDefinitions.CLOCK.get_fps()
        if fps == 0:
            fps = GameDefinitions.FPS

        return GameDefinitions.FPS


def dotget(count, scale=1.0, horizontal=True):
    if horizontal:
        factor = GameDefinitions.DOT_WIDTH
    else:
        factor = GameDefinitions.DOT_HEIGHT

    return int(count * factor * scale)


def get_img_theme_dir():
    return os.path.join('assets', 'img', GameDefinitions.THEME)


def get_shadow_resource():
    shadow_path = os.path.join(get_img_theme_dir(), 'shadow.png')
    if os.path.isfile(shadow_path):
        return shadow_path
    else:
        return None


def get_raw_game_speed(modifier=1, horizontal=False, fixed=False):
    if fixed:
        return GameDefinitions.get_frame_speed(horizontal) * modifier
    else:
        return GameDefinitions.get_game_speed(horizontal) * modifier


def get_level_based_speed(level, modifier=1, horizontal=False, fixed=False):
    r_speed = get_raw_game_speed(modifier, horizontal, fixed)
    game_speed = r_speed * (1 + 0.2 * level)
    return game_speed


def get_game_speed(level, march, modifier=1, horizontal=False, fixed=False):
    leveled_speed = get_level_based_speed(level, modifier, horizontal, fixed)
    speed = leveled_speed * (1 + 0.5 * march)
    return speed


def dotthemeres(resource):
    return os.path.join(get_img_theme_dir(), resource)


def dottextsize(font_size):
    division = GameDefinitions.DOT_WIDTH / float(32)
    font = int(font_size * division)
    return font + (font % 2)
