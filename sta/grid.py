import pygame

from sta.definitions import GameDefinitions, dotthemeres


class Grid(object):

    def __init__(self):
        self.resource_list = self.get_resources()
        self.grid = self.get_grid_definition()
        self.theme_enabled = True

    def get_resources(self):
        pass

    def get_grid_definition(self):
        pass

    def get_grid_width(self, scale=1):
        grid_width = len(self.grid[0]) if len(self.grid) > 0 else 0
        dot_width = int(GameDefinitions.DOT_WIDTH * scale)

        return grid_width * dot_width

    def get_grid_height(self, scale=1):
        grid_height = len(self.grid)
        dot_height = int(GameDefinitions.DOT_HEIGHT * scale)

        return grid_height * dot_height


class GenericGrid(Grid):

    def __init__(self, res, grid):
        self.grid_description = grid
        self.resources_description = res

        super(GenericGrid, self).__init__()

    def get_grid_definition(self):
        return self.grid_description

    def get_resources(self):
        return self.resources_description


class GridBuilder(object):

    @staticmethod
    def create_surface(grid_object, scale=1):
        grid = grid_object.grid
        resource_list = grid_object.resource_list

        grid_width = len(grid[0]) if len(grid) > 0 else 0
        grid_height = len(grid)

        dot_width = int(GameDefinitions.DOT_WIDTH * scale)
        dot_height = int(GameDefinitions.DOT_HEIGHT * scale)

        surface_width = grid_width * dot_width
        surface_height = grid_height * dot_height
        surface_dimensions = (surface_width, surface_height)

        surface = pygame.Surface(surface_dimensions, pygame.SRCALPHA, 32)
        base_surface = surface.convert_alpha()

        for line_index, line in enumerate(grid):
            for column_index, column in enumerate(line):
                r_element = column
                if r_element == 0:
                    pass
                else:
                    raw_asset = resource_list[r_element - 1]
                    if grid_object.theme_enabled:
                        element = dotthemeres(raw_asset)
                    else:
                        element = raw_asset

                    pos_x = column_index * dot_width
                    pos_y = line_index * dot_height

                    image = GridBuilder.create_image_from_resource(
                        element, dot_width, dot_height)

                    GridBuilder.draw_rect_to_surface(
                        base_surface, image, pos_x, pos_y)
        return base_surface

    @staticmethod
    def create_image_from_resource(resource, dot_width, dot_height):
        res = pygame.image.load(resource).convert_alpha()
        dot_dimension = (dot_width, dot_height)
        return pygame.transform.scale(res, dot_dimension).convert_alpha()

    @staticmethod
    def draw_rect_to_surface(surface, res, x, y):
        surface.blit(res, (x, y))
