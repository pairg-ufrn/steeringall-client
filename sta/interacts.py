import pygame
import sys
from pygame.locals import *

from sta.definitions import GameDefinitions
from sta.definitions.commands import GameCommands


class GameControllerWrapper(object):

    inverted = True
    invert_relation = None

    @staticmethod
    def init_wrapper():
        GameControllerWrapper.invert_relation = {
            # GameCommands.BOOST: GameCommands.BREAK,
            # GameCommands.BOOST_RELEASE: GameCommands.BREAK_RELEASE,
            # GameCommands.BREAK: GameCommands.BOOST,
            # GameCommands.BREAK_RELEASE: GameCommands.BOOST_RELEASE,

            GameCommands.LEFT: GameCommands.RIGHT,
            GameCommands.RIGHT: GameCommands.LEFT
        }

    @staticmethod
    def get_adjusted_input(game_input):
        if GameControllerWrapper.inverted:
            rel = GameControllerWrapper.invert_relation
            return rel[game_input] if game_input in rel.keys() else game_input

        return game_input


class Interact(object):

    def __init__(self):
        self.interact_name = 'Unknow Module'
        self.listener = None
        self.interact_enabled = True
        self.type = GameDefinitions.MODULES_NATURE_INTERACT
        self.started = False

    def name(self, name):
        self.interact_name = name
        return self

    def enabled(self, enabled):
        self.interact_enabled = enabled
        return self

    def get_enabled(self):
        return self.interact_enabled

    def get_name(self):
        return self.interact_name

    def set_listener(self, listener):
        self.listener = listener

    def notify_listener(self, input_result):
        game_input = GameControllerWrapper.get_adjusted_input(input_result)
        self.listener.handle_input(game_input)

    def check_interaction(self):
        pass

    def start(self):
        pass

    def stop(self):
        pass

    def startup(self):
        self.started = True
        self.start()

    def halt(self):
        self.started = False
        self.stop()


class InteractListener(object):

    def handle_input(self, game_input):
        pass


class EvtInteract(Interact):

    def __init__(self):
        super(EvtInteract, self).__init__()
        self.type = GameDefinitions.MODULES_NATURE_EVENT
        self.events = []

    def check_event_interaction(self, events):
        self.events = events
        self.check_interaction()


class KeyboardInteract(EvtInteract):

    def __init__(self):
        EvtInteract.__init__(self)
        self.going_right = False
        self.going_left = False

    def get_name(self):
        return "Keyboard interacts"

    def check_interaction(self):
        for event in self.events:

            if event.type == QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT and not self.going_left:
                    self.notify_listener(GameCommands.LEFT)
                    self.going_left = True

                if event.key == pygame.K_RIGHT and not self.going_right:
                    self.notify_listener(GameCommands.RIGHT)
                    self.going_right = True

                if event.key == pygame.K_UP or event.key == pygame.K_SPACE:
                    if event.key == pygame.K_SPACE:
                        self.notify_listener(GameCommands.CONTINUE)

                    self.notify_listener(GameCommands.BOOST)

                elif event.key == pygame.K_DOWN:
                    self.notify_listener(GameCommands.BREAK)

                if event.key == pygame.K_ESCAPE:
                    self.notify_listener(GameCommands.EXIT)

                if event.key == pygame.K_p:
                    self.notify_listener(GameCommands.PAUSE)

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    self.going_left = False

                if event.key == pygame.K_RIGHT:
                    self.going_right = False

                if event.key == pygame.K_UP or event.key == pygame.K_SPACE:
                    self.notify_listener(GameCommands.BOOST_RELEASE)

                if event.key == pygame.K_DOWN:
                    self.notify_listener(GameCommands.BREAK_RELEASE)
