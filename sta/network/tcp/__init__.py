import pickle
import socket


def create_client_socket(target_host, target_port):
    client = None
    try:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error:
        print 'Failed to create TCP socket'
        sys.exit()
    client.connect((target_host, target_port))

    return client


def receive_data(socket):
    f = socket.makefile('rb', 1024)
    data = pickle.load(f)
    f.close()
    return data


def send_data(socket, data):
    f = socket.makefile('wb', 1024)
    pickle.dump(data, f, 2)
    f.close()
