import socket
import sys


def create_udp_socket():
    my_socket = None
    try:
        my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    except socket.error:
        print 'Failed to create UDP socket'
        sys.exit()

    return my_socket


def send_data(s, message, target_host, target_port):
    try:
        s.sendto(message, (target_host, target_port))
    except socket.error, msg:
        print 'Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        sys.exit()


def receive_data(s):
    return s.recvfrom(1024)
