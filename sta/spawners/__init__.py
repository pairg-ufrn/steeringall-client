from sta import GameComponent
from sta.utils.rand import RandomPercentage, RandomSetSelection
from sta.utils.time import DistanceMarker


class GameSpawner(GameComponent):

    def __init__(self, game_descriptor):
        self.spawners = []
        self.selected_spawner = None
        self.game_descriptor = game_descriptor
        self.spare_timer = None
        self.spawn_enabled = True
        self.first_spawn = True
        self.alternate_spawner = None
        super(GameSpawner, self).__init__()

    def add_spawner(self, spawner):
        self.spawners.append(spawner)
        spawner.set_game_descriptor(self.game_descriptor)
        spawner.parent = self.parent
        return spawner

    def step(self):
        if not self.spawn_enabled:
            return

        if self.selected_spawner is None and len(self.spawners) == 0:
            return

        if self.selected_spawner is None:
            self.elect_spawner()

        if self.selected_spawner is None:
            return

        self.step_spawner()

    def elect_spawner(self):
        successful = []
        for spawner in self.spawners:
            percentage = RandomPercentage(spawner.chance_to_spawn)
            if percentage.is_successful():
                successful.append(spawner)

        success_count = len(successful)

        if success_count == 0:
            selection = self.alternate_spawner
        elif success_count == 1:
            selection = successful[0]
        else:
            selection = RandomSetSelection(successful).select()

        if selection is None:
            return

        if self.spare_timer is not None:
            selection.moment_counter = self.spare_timer
        self.selected_spawner = selection

    def step_spawner(self):
        if self.first_spawn:
            self.first_spawn = False
            self.selected_spawner.handle_first_spawn()

        if not self.selected_spawner.will_spawn():
            return

        self.selected_spawner.spawn(self.parent)
        spawner_marker = self.selected_spawner.distance_marker
        self.spare_timer = spawner_marker.distance_counter
        self.reset_spawner()

    def reset_spawner(self):
        self.selected_spawner = None


class SingleGameSpawner(GameSpawner):

    def __init__(self, spawner, game_descriptor):
        super(SingleGameSpawner, self).__init__(game_descriptor)
        self.selected_spawner = spawner
        spawner.set_game_descriptor(game_descriptor)

    def step(self):
        self.step_spawner()

    def reset_spawner(self):
        return


class GameComponentSpawner(object):

    def __init__(self, target, distance_to_spawn, chance_to_spawn, modifier=1):
        self.parent = None
        self.distance_to_spawn = distance_to_spawn
        self.chance_to_spawn = chance_to_spawn
        self.distance_marker = DistanceMarker(None, modifier)
        self.target = target
        self.modifier = modifier

    def handle_first_spawn(self):
        self.distance_marker.distance_counter = self.distance_to_spawn

    def set_game_descriptor(self, game_descriptor):
        self.distance_marker.game_descriptor = game_descriptor

    def will_spawn(self):
        self.distance_marker.step()
        if self.distance_marker.distance_counter >= self.distance_to_spawn:
            self.distance_marker.loop(self.distance_to_spawn)
            return True

        return False

    def spawn(self, world):
        pass