from sta.actors.road.border import DotBorder
from sta.behaviors import FallStepBehavior
from sta.bodies import BodyPositioner
from sta.definitions import dotget
from . import GameComponentSpawner


class BorderSpawner(GameComponentSpawner):

    def __init__(self, trail, game_descriptor):
        self.trail = trail
        self.game_descriptor = game_descriptor
        distance_to_spawn = DotBorder().get_height() + dotget(3)
        super(BorderSpawner, self).__init__(trail, distance_to_spawn, 100, 3)

    def spawn(self, world):
        right_border = world.add_child(DotBorder())
        left_border = world.add_child(DotBorder())

        left_border.x = self.trail.x
        left_border.y -= left_border.get_height()
        behavior = FallStepBehavior(
            self.game_descriptor, self.modifier, fixed=False)
        left_border.add_behavior(behavior)

        BodyPositioner.right_of(right_border, self.trail)
        right_border.x -= right_border.get_width()
        right_border.y -= right_border.get_height()
        right_border.add_behavior(behavior)
