from random import randrange

from sta.actors.vehicles import CarBehavior, DotCar, OnHeroCollideCarBehavior
from sta.actors.vehicles.ninja import DotNinja, NinjaBehavior
from sta.behaviors import DrawAlphaBehavior
from sta.behaviors import FallStepBehavior
from sta.utils.rand import RandomSelection, RandomPercentage
from . import GameComponentSpawner


class CarSpawner(GameComponentSpawner):

    def __init__(self, trail, game_descriptor, hero):
        self.trail = trail
        self.hero = hero
        self.game_descriptor = game_descriptor

        cars_between = 3
        distance_to_spawn = DotCar().get_height() * (1 + cars_between)
        super(CarSpawner, self).__init__(trail, distance_to_spawn, 100, 1)

    def spawn(self, world):
        max_trail = self.trail.trail_count
        possible_trails = range(0, max_trail)

        quantity_dice = RandomSelection(1, max_trail - 2)
        clone_numbers = quantity_dice.select()

        has_ninja = RandomPercentage(25)
        if has_ninja.is_successful():
            ninjas = RandomSelection(0, clone_numbers)
            ninja_count = quantity_dice.select()
        else:
            ninja_count = 0

        for i in range(0, clone_numbers):
            if ninja_count > 0:
                enemy = world.add_child(DotNinja())
                enemy.add_behavior(DrawAlphaBehavior())
                enemy.add_behavior(NinjaBehavior())
                ninja_count -= 1
            else:
                enemy = world.add_child(DotCar())
            # enemy = world.add_child(self.get_enemy())

            random_minus_y = randrange(10, 25)
            minus_y = random_minus_y / float(10)
            # minus_y = 0

            enemy.y -= enemy.get_height() * (1 + minus_y)

            random_index = randrange(0, len(possible_trails))
            elected_trail = possible_trails.pop(random_index)

            speed_chance = RandomSelection(10, 13)
            speed_mod = float(speed_chance.select() / float(10))
            # speed_mod = 1

            enemy.add_behavior(CarBehavior(self.trail, elected_trail))

            enemy.add_behavior(FallStepBehavior(
                self.game_descriptor, speed_mod))

            enemy.add_behavior(OnHeroCollideCarBehavior(
                self.hero, self.game_descriptor, self.parent))

    def get_enemy(self):
        return DotCar()
