from random import randrange

from sta.actors.collectibles.coin import Coin, CoinBehavior
from sta.actors.road import DotBorder
from sta.behaviors import FallStepBehavior
from sta.definitions import dotget
from sta.spawners import GameComponentSpawner


class CoinSpawner(GameComponentSpawner):

    def __init__(self, road, hero, game_descriptor):
        self.hero = hero
        self.road = road
        self.game_descriptor = game_descriptor
        area_height = dotget(4)
        distance_to_spawn = area_height * (1 + 1)
        super(CoinSpawner, self).__init__(road, distance_to_spawn, 90, 0.5)

    def spawn(self, world):
        max_trail = self.road.trail_count
        possible_trails = range(0, max_trail)

        random_index = randrange(0, len(possible_trails))
        elected_trail = possible_trails.pop(random_index)

        enemy = Coin()
        enemy.x = self.road.x + DotBorder().get_width()
        enemy.x += (elected_trail * dotget(3)) + dotget(1)
        enemy.y = -enemy.get_height()

        enemy.add_behavior(FallStepBehavior(self.game_descriptor, 0.5))
        enemy.add_behavior(CoinBehavior(self.hero, self.game_descriptor))

        world.add_child(enemy)
