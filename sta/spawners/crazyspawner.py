from random import randrange

from sta.actors.vehicles import CarBehavior, DotCar, OnHeroCollideCarBehavior
from sta.actors.vehicles.crazy import DotCrazy, CrazyBehavior
from sta.behaviors import FallStepBehavior
from sta.utils.rand import RandomSelection
from .carspawner import CarSpawner


class CrazySpawner(CarSpawner):

    def __init__(self, trail, game_descriptor, hero):
        self.trail = trail
        self.hero = hero
        self.game_descriptor = game_descriptor
        distance_to_spawn = DotCar().get_height() * (1 + 4)
        super(CarSpawner, self).__init__(trail, distance_to_spawn, 30, 1)

    def spawn(self, world):
        max_trail = self.trail.trail_count
        enemy = world.add_child(self.get_enemy())
        enemy.y -= enemy.get_height()

        elected_trail = randrange(0, max_trail)

        speed_chance = RandomSelection(10, 15)
        speed_mod = float(speed_chance.select() / float(10))

        enemy.add_behavior(CarBehavior(self.trail, elected_trail))
        enemy.add_behavior(FallStepBehavior(self.game_descriptor, speed_mod))

        enemy.add_behavior(OnHeroCollideCarBehavior(
            self.hero, self.game_descriptor, self.parent))

        enemy.add_behavior(
            CrazyBehavior(self.game_descriptor, self.trail))

    def get_enemy(self):
        return DotCrazy()
