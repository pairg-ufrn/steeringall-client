from sta.actors.areas.inverted import InvertedArea, InvertedAreaBehavior
from sta.actors.road import DotBorder
from sta.behaviors import DrawAlphaBehavior, FallStepBehavior
from sta.definitions import dotget
from sta.spawners import GameComponentSpawner


class InvertedAreaSpawner(GameComponentSpawner):

    def __init__(self, road, hero, game_descriptor):
        self.hero = hero
        self.road = road
        self.game_descriptor = game_descriptor
        area_height = dotget(6)
        distance_to_spawn = area_height * (1 + 8)
        super(InvertedAreaSpawner, self).__init__(road, distance_to_spawn, 10, 1)

    def spawn(self, world):
        max_trail = self.road.trail_count
        enemy = InvertedArea(max_trail * 3, 20)
        enemy.x = self.road.x + DotBorder().get_width()
        enemy.y = -enemy.get_height()

        enemy.add_behavior(DrawAlphaBehavior())
        enemy.add_behavior(FallStepBehavior(self.game_descriptor))
        enemy.add_behavior(InvertedAreaBehavior(self.hero))

        enemy.set_value('alpha', 80)
        world.add_child(enemy)
