from sta.actors.dotradar import DotRadar, RadarBehavior
from sta.actors.road.border import DotBorder
from sta.behaviors import DrawAlphaBehavior
from sta.behaviors import FallStepBehavior
from sta.definitions import dotget
from . import GameComponentSpawner


class RadarSpawner(GameComponentSpawner):

    def __init__(self, world, road, game_descriptor, hero):
        self.road = road
        self.game_descriptor = game_descriptor
        self.hero = hero
        self.world = world
        area_height = dotget(6)
        distance_to_spawn = area_height * (1 + 3)
        super(RadarSpawner, self).__init__(road, distance_to_spawn, 50, 1)

    def spawn(self, world):
        max_trail = self.road.trail_count
        enemy = DotRadar(max_trail * 3)
        enemy.x = self.road.x + DotBorder().get_width()
        enemy.y = -enemy.get_height()

        enemy.add_behavior(DrawAlphaBehavior())
        enemy.add_behavior(FallStepBehavior(self.game_descriptor))
        enemy.add_behavior(RadarBehavior(
            self.hero, self.game_descriptor, self.world, self.road))

        enemy.set_value('alpha', 80)
        world.add_child(enemy)
