import os
import pygame

from sta.definitions import GameDefinitions


class SoundStorage(object):
    sound_library = {}

    @staticmethod
    def get_sound(path):
        if not GameDefinitions.SOUND_ENABLED:
            return None

        sound = SoundStorage.sound_library.get(path)
        if sound is None:
            path = path.replace('/', os.sep).replace('\\', os.sep)
            sound = pygame.mixer.Sound(path)
            SoundStorage.sound_library[path] = sound

        return sound

    @staticmethod
    def play_sound(path):
        sound = SoundStorage.get_sound(path)
        if sound is not None:
            sound.play()



class SurfaceStorage(object):
    res_library = {}

    @staticmethod
    def set_res(class_name, surface):
        SurfaceStorage.res_library.update({class_name: surface})

    @staticmethod
    def get_res(class_name):
        if class_name in SurfaceStorage.res_library:
            return SurfaceStorage.res_library[class_name]

        return None

    @staticmethod
    def flush():
        SurfaceStorage.res_library = {}
