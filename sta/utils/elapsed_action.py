import time


class ElapsedAction(object):

    def __init__(self, time_to_update=0.75):
        self.last_elapsed = None
        self.counter = 0
        self.time_to_update = time_to_update
        self.is_running = False

    def start(self):
        if not self.is_running:
            self.is_running = True
            self.last_elapsed = time.time()

    def tick(self):
        if not self.is_running:
            self.start()
            return True
        difference = time.time() - self.last_elapsed
        self.last_elapsed = time.time()

        self.counter += difference

        time_to_tick = (self.counter > self.time_to_update)
        if time_to_tick:
            self.stop()
            return False

        return time_to_tick

    def stop(self):
        self.counter = 0
        self.is_running = False
