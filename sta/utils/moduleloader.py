import importlib
import sys
from os import getcwd, listdir
from os.path import isdir, join


from sta.definitions import *
from sta.interacts import KeyboardInteract


class ModuleLoader(object):

    def __init__(self):
        sys.path.insert(0, getcwd())
        self.working_dir = GameDefinitions.MODULES_DIR

    def load_modules(self):
        loaded_modules = [KeyboardInteract()]
        onlydirs = []
        for f in listdir(self.working_dir):
            if isdir(join(self.working_dir, f)):
                onlydirs.append(f)

        for module_dir in onlydirs:
            if module_dir == '.git':
                continue

            try:
                module = importlib.import_module("modules.%s" % module_dir)
            except ImportError:
                print("Module skipped by import error")
                continue
            interact = module.exports()
            loaded_modules.append(interact)

        return loaded_modules
