import random


class RandomPercentage(object):

    def __init__(self, chance):
        self.chance = chance

    def set_chance(self, chance):
        self.chance = chance

    def is_successful(self):
        selected = self.get_percentage()
        if selected <= self.chance:
            return True
        else:
            return False

    @staticmethod
    def get_percentage():
        return random.randint(0, 100)


class RandomSelection(object):

    def __init__(self, start, end):
        self.start = start
        self.end = end

    def select(self):
        return random.randint(self.start, self.end)


class RandomSetSelection(RandomSelection):

    def __init__(self, obj_set):
        self.obj_set = obj_set
        super(RandomSetSelection, self).__init__(0, len(obj_set) - 1)

    def select(self):
        selection = super(RandomSetSelection, self).select()
        return self.obj_set[selection]
