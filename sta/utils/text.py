import pygame

from sta.bodies import GameBody
from sta.definitions import GameDefinitions


class DotText(GameBody):

    def __init__(self, text):
        super(DotText, self).__init__()
        self.text = text
        self.size = 32
        self.text_color = (0, 0, 0)
        self.bg_color = GameDefinitions.FILL_COLOR
        self.font = "assets/fonts/expressway_rg.ttf"

    def set_text(self, text):
        self.text = text
        return self

    def set_size(self, size):
        self.size = size
        return self

    def set_text_color(self, text_color):
        self.text_color = text_color
        return self

    def set_bg_color(self, bg_color):
        self.bg_color = bg_color
        return self

    def set_font(self, font):
        self.font = font
        return self

    def wrap(self):
        # if self.surface is not None:
        #     self.surface.fill(self.bg_color)

        font_obj = pygame.font.Font(self.font, self.size)
        text_obj = font_obj.render(self.text, True, self.text_color)

        self.width = text_obj.get_width()
        self.height = text_obj.get_height()

        if self.bg_color is None:
            self.surface = text_obj.convert_alpha()
            return self

        surf = pygame.Surface(font_obj.size(self.text), pygame.SRCALPHA, 32)
        if self.bg_color is not None:
            surf.fill(self.bg_color)
        self.surface = surf.convert_alpha()
        self.surface.blit(text_obj, (0, 0))
        return self

    # def set_text(self, text, size=32, text_color=(0, 0, 0),
    #              bg_color=(255, 255, 255),
    #              font="assets/fonts/expressway_rg.ttf"):
    #     if self.surface is not None:
    #         self.surface.fill(bg_color)
    #
    #     font_obj = pygame.font.Font(font, size)
    #     text_obj = font_obj.render(text, True, text_color)
    #
    #     self.width = text_obj.get_width()
    #     self.height = text_obj.get_height()
    #
    #     super(DotText, self).__init__(self.width, self.height)
    #     surf = pygame.Surface(font_obj.size(text), pygame.SRCALPHA, 32)
    #     surf.fill(bg_color)
    #     self.surface = surf.convert()
    #     self.surface.blit(text_obj, (0, 0))
