from decimal import *

from sta.definitions import GameDefinitions, get_game_speed


class Timer(object):

    def __init__(self):
        self.counter = 0

    def reset(self):
        self.counter = 0

    def loop(self, limit):
        self.counter -= limit

    def step(self):
        frame_speed = Decimal(1) / GameDefinitions.get_current_fps()
        self.counter += frame_speed


class DistanceMarker(object):

    def __init__(self, game_descriptor, modifier=1):
        self.game_descriptor = game_descriptor
        self.distance_counter = 0
        self.modifier = modifier

    def step(self):
        level = self.game_descriptor.level
        march = self.game_descriptor.march
        modifier = self.modifier
        self.distance_counter += get_game_speed(level, march, modifier)

    def reset(self):
        self.distance_counter = 0

    def loop(self, limit):
        self.distance_counter -= limit
