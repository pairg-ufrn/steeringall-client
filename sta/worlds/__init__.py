from sta import GameComponentCollection
from sta.definitions.commands import GameCommands


class DotWorld(GameComponentCollection):

    def __init__(self):
        self.screen = None
        self.init_args = {}
        super(DotWorld, self).__init__()

    def handle_input(self, game_input):
        if game_input == GameCommands.EXIT:
            self.screen.turn_off()

        super(DotWorld, self).handle_input(game_input)

    def on_set_screen(self):
        self.boot()

    def boot(self):
        pass

    def _step(self):
        self.step()

    def _draw(self, displaysurf):
        self.draw(self.screen.displaysurf)

    def update(self):
        self._step()
        self._draw(self.screen.displaysurf)


class DotPausableWorld(DotWorld):

    def __init__(self):
        self.paused = False
        self.pausable = True
        self.secondary_children = []
        super(DotPausableWorld, self).__init__()

    def add_secondary_child(self, child):
        child.parent = self
        self.secondary_children.append(child)

        return child

    def remove_secondary_child(self, child):
        self.secondary_children.remove(child)

    def trigger_pause_event(self):
        if self.paused:
            self.on_pause()
        else:
            self.on_resume()

    def on_set_screen(self):
        super(DotPausableWorld, self).on_set_screen()
        self.paused_boot()

    def handle_input(self, game_input):
        if game_input == GameCommands.EXIT:
            self.screen.turn_off()

        if game_input == GameCommands.PAUSE and self.pausable:
            self.paused = not self.paused
            self.trigger_pause_event()

        if self.paused:
            self.paused_handle_input(game_input)
            return

        super(DotWorld, self).handle_input(game_input)

    def _step(self):
        if self.paused:
            self.paused_step()
        else:
            self.step()

    def _draw(self, displaysurf):
        if self.paused:
            self.paused_draw(displaysurf)
        else:
            self.draw(displaysurf)

    def on_pause(self):
        pass

    def on_resume(self):
        pass

    def paused_boot(self):
        pass

    def paused_handle_input(self, game_input):
        pass

    def paused_step(self):
        pass

    def paused_draw(self, displaysurf):
        for child in self.secondary_children:
            child.draw(displaysurf)
