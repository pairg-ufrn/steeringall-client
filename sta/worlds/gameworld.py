import pygame

from sta.actors import GameDescriptor
from sta.actors.game_observers import DistanceScoreObserver, LevelObserver, InvencibleObserver
from sta.actors.hud import DotScoreAndLevelHUD
from sta.actors.hud.dotlifeometer import DotLifeOMeter
from sta.actors.hud.dotspeedometer import DotSpeedOMeter
from sta.actors.road import DotRoad
from sta.actors.vehicles.hero import DotHero, HeroBehavior, DotShield, AddonBehavior
from sta.behaviors import DrawAlphaBehavior
from sta.bodies import BodyPositioner
from sta.definitions import dotget, GameDefinitions
from sta.definitions.commands import GameCommands
from sta.interacts import GameControllerWrapper
from sta.spawners import GameSpawner, SingleGameSpawner, GameComponentSpawner
from sta.spawners.borderspawner import BorderSpawner
from sta.spawners.carspawner import CarSpawner
from sta.spawners.crazyspawner import CrazySpawner
from sta.spawners.invertedspawner import InvertedAreaSpawner
from sta.spawners.policespawner import PoliceSpawner
from sta.spawners.radarspawner import RadarSpawner
from sta.spawners.coinspawner import CoinSpawner
from sta.spawners.shieldspawner import ShieldSpawner
from sta.storages import SoundStorage
from sta.utils.text import DotText
from sta.utils.time import Timer
from sta.worlds import DotPausableWorld

class GameWorld(DotPausableWorld):

    def __init__(self):
        super(GameWorld, self).__init__()
        GameControllerWrapper.inverted = False
        self.hero = None
        self.resumed = True
        self.resume_text = DotText("")
        self.resume_last = -1
        self.timer = Timer()
        self.collided = False
        self.descriptor = None

    def boot(self):
        if 'game_descriptor' in self.init_args:
            self.descriptor = self.add_child(self.init_args['game_descriptor'])
        else:
            self.descriptor = self.add_child(GameDescriptor())

        descriptor = self.descriptor

        music_enabled = GameDefinitions.SOUND_ENABLED
        if descriptor.get_value('music_started') is None and music_enabled:
            pygame.mixer.music.load("assets/music/racing-theme.mp3")
            pygame.mixer.music.set_volume(0.2)
            pygame.mixer.music.play(-1, 0.0)
            descriptor.set_value('music_started', True)

        road = self.add_child(DotRoad(7))
        BodyPositioner.margin_left(road, dotget(8))

        self.add_child(DistanceScoreObserver(descriptor))
        self.add_child(LevelObserver(descriptor))

        hero = DotHero()
        self.hero = hero
        BodyPositioner.margin_bottom(hero, dotget(1.5))
        BodyPositioner.align_bottom(hero)

        hero.add_behavior(HeroBehavior(road))

        dot_shield = DotShield()
        dot_shield.set_value('target', hero)
        dot_shield.set_value('x_offset', 0)
        dot_shield.set_value('y_offset', (dotget(1) * -1))
        dot_shield.add_behavior(AddonBehavior())
        dot_shield.add_behavior(DrawAlphaBehavior())

        dot_shield.visible = False

        invencible_observer = self.add_child(InvencibleObserver(dot_shield))
        descriptor.set_value('invencible_observer', invencible_observer)

        self.add_child(dot_shield)

        speedometer = self.add_child(DotSpeedOMeter(descriptor, 1))
        BodyPositioner.margin_left(speedometer, dotget(1))
        BodyPositioner.margin_bottom(speedometer, dotget(1))
        BodyPositioner.right_of(speedometer, road)
        BodyPositioner.align_bottom(speedometer)

        # embed = self.add_child(DotEmbedSpeedOMeter(descriptor, hero, 0.5))

        lifeometer = self.add_child(DotLifeOMeter(descriptor))
        BodyPositioner.margin_left(lifeometer, dotget(2))
        BodyPositioner.margin_bottom(lifeometer, dotget(1))
        BodyPositioner.right_of(lifeometer, speedometer)
        BodyPositioner.align_bottom(lifeometer)

        level_hud = self.add_child(DotScoreAndLevelHUD(descriptor))
        BodyPositioner.margin_left(level_hud, dotget(2))
        BodyPositioner.right_of(level_hud, lifeometer)
        level_hud.y = lifeometer.y - level_hud.get_height()

        self.add_child(SingleGameSpawner(
            BorderSpawner(road, descriptor), descriptor))

        spawner = self.add_child(GameSpawner(descriptor))
        spawner.add_spawner(CarSpawner(road, descriptor, hero))
        spawner.add_spawner(CrazySpawner(road, descriptor, hero))
        spawner.add_spawner(PoliceSpawner(road, descriptor, hero, spawner))
        spawner.add_spawner(RadarSpawner(self, road, descriptor, hero))

        area_spawner = self.add_child((GameSpawner(descriptor)))
        empty_spawner = GameComponentSpawner(
            None, (dotget(GameDefinitions.WINDOW_HEIGHT) / 2), 0)
        empty_spawner.set_game_descriptor(descriptor)
        area_spawner.alternate_spawner = empty_spawner
        area_spawner.add_spawner(InvertedAreaSpawner(road, hero, descriptor))

        collectibles_spawner = self.add_child(GameSpawner(descriptor))
        empty_spawner = GameComponentSpawner(
            None, dotget(GameDefinitions.WINDOW_HEIGHT), 0)

        empty_spawner.set_game_descriptor(descriptor)

        collectibles_spawner.alternate_spawner = empty_spawner

        collectibles_spawner.add_spawner(CoinSpawner(road, hero, descriptor))
        collectibles_spawner.add_spawner(ShieldSpawner(road, hero, descriptor))

        self.add_child(hero)

    def paused_boot(self):
        text = self.add_secondary_child(DotText("Game Paused")).wrap()
        BodyPositioner.center_x(text)
        BodyPositioner.center_y(text)

    def handle_input(self, game_input):
        super(GameWorld, self).handle_input(game_input)
        if self.collided:
            if game_input == GameCommands.CONTINUE:
                GameControllerWrapper.inverted = False
                self.screen.set_world('game', {
                    'game_descriptor': self.descriptor
                })

    def on_pause(self):
        pygame.mixer.music.pause()


    def on_resume(self):
        self.add_child(self.resume_text)
        self.resumed = False
        self.resume_text.visible = True
        self.timer.reset()

    def on_collide(self):
        if self.descriptor.lives == 0:
            pygame.mixer.music.stop()
            self.screen.set_world('game-over', {
                'game_descriptor': self.descriptor
            })
        self.collided = True

    def draw(self, displaysurf):
        hero_was_visible = self.hero.visible
        self.hero.visible = False
        super(GameWorld, self).draw(displaysurf)

        if hero_was_visible:
            self.hero.visible = True
            self.hero.draw(displaysurf)

        self.hero.visible = hero_was_visible

    def step(self):
        if self.collided:
            return

        if self.resumed:
            super(GameWorld, self).step()
        else:
            self.timer.step()
            if self.timer.counter < 4:
                int_moment = int(self.timer.counter)
                int_text = 3 - int_moment
                if int_text == 0:
                    if self.resume_last != int_text:
                        SoundStorage.play_sound('assets/music/effects/go.wav')
                        my_text = "GO!"
                        self.resume_text.set_text(my_text).wrap()
                        self.resume_last = int_text
                else:
                    if self.resume_last != int_text:
                        SoundStorage.play_sound('assets/music/effects/counting.wav')
                        my_text = str(int_text)
                        self.resume_text.set_text(my_text).wrap()
                        self.resume_last = int_text

                BodyPositioner.center_x(self.resume_text)
                BodyPositioner.center_y(self.resume_text)

            else:
                self.resume_text.visible = False
                self.resumed = True
                self.remove_child(self.resume_text)
                pygame.mixer.music.unpause()
