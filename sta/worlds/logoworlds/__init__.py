from sta.definitions.commands import GameCommands

from sta.worlds import DotWorld


class FadeInFadeOutWorld(DotWorld):

    def __init__(self):
        super(FadeInFadeOutWorld, self).__init__()
        self.alpha = 0
        self.anim_state = 1
        self.animation_counter = 0
        self.active = True

    def init_variables(self, args={}):
        self.alpha = 0
        self.anim_state = 1
        self.animation_counter = 0
        self.active = True

    def handle_input(self, game_input):
        super(FadeInFadeOutWorld, self).handle_input(game_input)
        if game_input == GameCommands.CONTINUE:
            self.pause()

    def step(self):
        super(FadeInFadeOutWorld, self).step()
        if self.active:
            self.change_alpha()
            self.animation_counter += 1

            if self.anim_state == 1:
                self.alpha += 10

                if self.alpha > 255:
                    self.anim_state = 2
                    self.animation_counter = 0

            if self.anim_state == 2:
                self.animation_counter += 1
                if self.animation_counter > self.screen.fps * 4:
                    self.anim_state = 3

            if self.anim_state == 3:
                self.alpha -= 10
                if self.alpha <= 0:
                    self.pause()

        else:
            self.change_world()
            del self

    def pause(self):
        self.active = False

    def change_alpha(self):
        if self.alpha > 255:
            given_alpha = 255
        else:
            given_alpha = self.alpha

        for child in self.children:
            child.set_value('alpha', given_alpha)

    def on_set_screen(self):
        pass

    def change_world(self):
        pass
