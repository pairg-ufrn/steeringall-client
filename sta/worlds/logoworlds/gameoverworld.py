from sta.behaviors import DrawAlphaBehavior
from sta.bodies import BodyPositioner
from sta.definitions import dotget, dottextsize
from sta.utils.text import DotText
from . import FadeInFadeOutWorld


class GameOverWorld(FadeInFadeOutWorld):

    def on_set_screen(self):
        game_descriptor = self.init_args['game_descriptor']

        label_text = "Game Over"
        sublabel_text = "Final score: " + str(game_descriptor.score)

        label = self.add_child(DotText(label_text))
        sublabel = self.add_child(DotText(sublabel_text))
        label.set_size(
            dottextsize(32)).set_font(
                "assets/fonts/8-BIT-WONDER.ttf").wrap()
        BodyPositioner.center_x(label)
        BodyPositioner.center_y(label)

        sublabel.set_size(dottextsize(18)).wrap()
        BodyPositioner.center_x(sublabel)
        BodyPositioner.margin_top(sublabel, dotget(1, horizontal=False))
        BodyPositioner.below(sublabel, label)

        label.add_behavior(DrawAlphaBehavior())
        sublabel.add_behavior(DrawAlphaBehavior())

    def change_world(self):
        self.screen.set_world('title')
