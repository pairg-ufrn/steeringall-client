import i18n
from sta.actors.dotpairg import DotPairg
from sta.behaviors import DrawAlphaBehavior
from sta.bodies import BodyPositioner
from sta.definitions import dotget, dottextsize
from sta.utils.text import DotText
from . import FadeInFadeOutWorld

_ = i18n.language.ugettext



class PairgLogoWorld(FadeInFadeOutWorld):

    def on_set_screen(self):
        label_text = "PAIRG - Physical Artifacts of Interaction Research Group"
        sublabel_text = _("Developed by") + " Alison Bento"

        logo = self.add_child(DotPairg())
        label = self.add_child(DotText(label_text))
        sublabel = self.add_child(DotText(sublabel_text))

        BodyPositioner.center_x(logo)
        BodyPositioner.center_y(logo)

        label.set_size(dottextsize(32)).wrap()
        BodyPositioner.center_x(label)
        BodyPositioner.margin_top(label, dotget(1, horizontal=False))
        BodyPositioner.below(label, logo)

        sublabel.set_size(dottextsize(18)).wrap()
        BodyPositioner.center_x(sublabel)
        BodyPositioner.margin_top(sublabel, dotget(1, horizontal=False))
        BodyPositioner.below(sublabel, label)

        label.add_behavior(DrawAlphaBehavior())
        sublabel.add_behavior(DrawAlphaBehavior())

    def change_world(self):
        self.screen.set_world('ufrn')
