from sta.actors.dotufrn import DotUFRN
from sta.behaviors import DrawAlphaBehavior
from sta.bodies import BodyPositioner
from sta.definitions import dotget, dottextsize
from sta.utils.text import DotText
from . import FadeInFadeOutWorld


class UfrnLogoWorld(FadeInFadeOutWorld):

    def on_set_screen(self):
        logo = self.add_child(DotUFRN())
        label = self.add_child(
            DotText(_("Universidade Federal do Rio Grande do Norte")))

        BodyPositioner.center_x(logo)
        BodyPositioner.center_y(logo)

        label.set_size(dottextsize(32)).wrap()
        BodyPositioner.center_x(label)
        BodyPositioner.margin_top(label, dotget(1, horizontal=False))
        BodyPositioner.below(label, logo)

        label.add_behavior(DrawAlphaBehavior())

    def change_world(self):
        self.screen.set_world('title')
