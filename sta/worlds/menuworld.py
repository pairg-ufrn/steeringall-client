import i18n
from sta.actors.dotgoblet import DotGoblet
from sta.bodies import BodyPositioner
from sta.definitions import dotget, dottextsize
from sta.definitions.commands import GameCommands
from sta.storages import SoundStorage
from sta.utils.text import DotText
from sta.worlds import DotWorld

_ = i18n.language.ugettext


class TitleWorld(DotWorld):

    def __init__(self):
        super(TitleWorld, self).__init__()
        self.is_pressing = False

    def on_set_screen(self):
        logo = self.add_child(DotText("Steering All"))
        goblet = self.add_child(DotGoblet())
        press_start = self.add_child(DotText(_("Press <space> to continue")))

        logo.set_font("assets/fonts/8-BIT-WONDER.ttf").wrap()
        BodyPositioner.margin_top(logo, dotget(1))
        BodyPositioner.center_x(logo)

        BodyPositioner.center_x(goblet)
        BodyPositioner.center_y(goblet)

        press_start.set_size(dottextsize(22)).wrap()
        BodyPositioner.margin_bottom(press_start, dotget(1))
        BodyPositioner.align_bottom(press_start)
        BodyPositioner.center_x(press_start)

    def handle_input(self, game_input):
        super(TitleWorld, self).handle_input(game_input)

        if game_input == GameCommands.CONTINUE:
            self.change_world()
        #     self.is_pressing = True
        #
        # is_unboost = (game_input == GameCommands.BOOST_RELEASE)
        # if is_unboost and self.is_pressing:
        #     self.change_world()

    def change_world(self):
        SoundStorage.play_sound('assets/music/effects/levelup.wav')
        self.screen.set_world('game')
        del self
        # # world = DotGame(3, 3, 0, 1, True)
        # try:
        #     tcp_socket = create_client_socket(
        #         GameDefinitions.SERVER_ADDRESS,
        #         GameDefinitions.SERVER_PORT)
        #
        #     send_data(tcp_socket, {'do': 'match'})
        #     self.label.set_text("...Connecting...", 32)
        #     self.step()
        #
        #     response = receive_data(tcp_socket)
        #     print response
        #
        #     player_id = response['player']
        #     match_id = response['match']
        #
        #     socket = create_udp_socket()
        #     world.set_network_properties(socket, player_id, match_id)
        # except Exception:
        #     pass
        # finally:
        #    self.screen.set_world(world)
        #    del self
