import os
import shutil
import sys


def main():
    check_errors()

    print("Building...")
    version = sys.argv[1]

    needs_rebuild = False
    if os.name == 'nt':
        subdir = 'win'
        needs_rebuild = True
    else:
        subdir = 'linux'

    version_path = os.path.join(os.getcwd(), 'release',
                                'pyinstaller', subdir, version)

    build_path = os.path.join(version_path, 'work')
    dist_path = os.path.join(version_path, 'dist')

    steering_all_dir = os.getcwd()
    steering_all_exec = os.path.join(steering_all_dir, 'steering_all.py')

    os.mkdir(version_path)
    os.mkdir(build_path)
    os.mkdir(dist_path)

    run_pyinstaller(steering_all_exec, version_path,
                    build_path, dist_path, '--onefile')

    assets_dir = os.path.join(steering_all_dir, 'assets')
    assets_dist_dir = os.path.join(dist_path, 'assets')
    shutil.copytree(assets_dir, assets_dist_dir)

    modules_dir = os.path.join(dist_path, 'modules')
    os.mkdir(modules_dir)
    open(os.path.join(modules_dir, '__init__.py'), 'a').close()

    if needs_rebuild:
        re_version_path = os.path.join(version_path, 'single')
        re_build_path = os.path.join(re_version_path, 'work')
        re_dist_path = os.path.join(re_version_path, 'dist')

        os.mkdir(re_version_path)
        os.mkdir(re_build_path)
        os.mkdir(re_dist_path)

        run_pyinstaller(steering_all_exec, re_version_path,
                        re_build_path, re_dist_path, '--onedir')

        manf_file = os.path.join(re_dist_path, 'steering_all', 'steering_all.exe.manifest')
        manf_dest_file = os.path.join(dist_path, 'steering_all.exe.manifest')
        shutil.copy2(manf_file, manf_dest_file)

    # exec_file = os.path.join(steering_all_dir, '_run.sh')
    # exec_dest_file = os.path.join(dist_path, 'game.sh')
    # shutil.copy2(exec_file, exec_dest_file)

    print("Done")


def run_pyinstaller(steering_all_exec, version_path,
                    build_path, dist_path, prefix):
    command = "pyinstaller "
    command += "%s --specpath %s --workpath %s --distpath %s" % (
        steering_all_exec,
        version_path,
        build_path,
        dist_path)

    command += ' ' + prefix

    os.system(command)


def check_errors():

    if len(sys.argv) < 1:
        print("ERROR: Version unespecified")
        exit()


if __name__ == "__main__":
    main()
